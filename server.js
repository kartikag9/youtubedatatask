const { request } = require('express');
const express = require('express');
const mongoose = require('mongoose'); 
const morgan = require('morgan');
const cors = require('cors');
const storage = require('node-persist');
 
// function to set a dummy value inside local node-persist cache....
// var xyz = async() => {
//     const myStorage = storage.create({dir: 'myDir', ttl: 300000});
//     await myStorage.init();
//     myStorage.setItem('key','AIzaSyD50K9Xg5lv1xe2IGQC-qWWNrKx1W3MuDw')
//    console.log(await myStorage.getItem('key')); // yourname
// }
// xyz();

const videoDataRouter = require('./routes/videoData');
const videoDataSchema = require('./models/videoData');

var app = express();

app.set('view-engine', 'ejs');

app.use(morgan("combined"));
app.use(cors());

let mongoUri = "mongodb+srv://kartik:K%40rtik99@cluster0-yilk1.mongodb.net/test?retryWrites=true&w=majority";
mongoose.connect(mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
})
.then(()=> {
    console.log('DB CONNECTED');
})
.catch((err)=> {
    console.log("DB CONNECTION ERROR " + err);
});

app.use('/api', videoDataRouter);
// this is the route to the dashboard containing all the data, 
// PS: just tried a hand with frontend 😅
app.get('/dashboard',(req,res)=>{
    videoDataSchema.find({}).sort(-"publisedAt").exec((err,docs)=>{
        if(err) {
            return res.json({"error": err})
        }
        res.render('data.ejs',{data:docs});
    });
});
app.use('*', (request,response)=> {
    return response.status(404).json({"err":"page not found"});
});

// callig the dataFetcher core, which fetches data from youtube API and export it to database (MongoDB).
var dataFetcher = require('./core/dataFetcher');

var PORT = process.env.PORT || 3002;
app.listen(PORT, ()=> {
      console.log(`app up and running on port ${PORT}`);
});
