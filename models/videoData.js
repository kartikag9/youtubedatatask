// This collection stores the video data which is being fetched through the youtube API into the database (MongoDB).

var mongoose = require('mongoose');

var videoDataSchema = new mongoose.Schema({
    title: {
        // the title of video
        type: String,
        unique: true
    },
    description: {
        // the description of video
        type: String
    },
    publishedAt: {
        // date and time of video publish
        type: Date
    },
    thumbnails: {
        // thumbnails details
        type: Object
    }
},{timestamps: true});

module.exports = mongoose.model('videoData', videoDataSchema);