// this collection is being used to store in new and unused api keys for youtube. It is being considered as a store room.
// the api key being used currently will be taken out of the database and will be stored into the local node-persist cache storage.

var mongoose = require('mongoose');

var apiKeySchema = new mongoose.Schema({
    key: {
        type: String
        // this is the api key
    },
    SNO: {
        type: String
        // serial number for reference
    }
},{timestamps: true});

module.exports = mongoose.model('apiKey', apiKeySchema);