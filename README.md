# Youtube Data Project

Youtube Data Project is a Node.js based application which fetches data from youtube continuously after a fixed interval of time, and exports the data to DB (Here, MongoDB) and we can search for videos in the DB based on keywords matching with either the title or description of the video.

## Tech

- Node.js (Express)
- MongoDB
- node-persist (as a cache)
- Youtube Data API v3
- EJS (for views)
- Docker

## Installation

Use the package manager [npm] to install dependencies.

```bash
npm install
```

## Usage

```node
npm start
```
OR
```node
node server.js
```

## Project Overview
- In this application, after every 10 seconds, youtube search API is hit to fetch videos on a pre-defined query (here, CRICKET). Then stores all the data of videos in the database.
- The quota of API key might get exhausted. So Have implemented a process to change the API key. API key is being stored in the local cache (here, node-persist library). Once the quota gets exhausted, automatically new API key is fetched from DB and stored into the local cache.
- For the search, Regex has been used to generate a regular expression based on the searchQuery which can fetch related title/description videos from the DB.
Dashboard has been implemented to display all the results, due to time crunch have not implemented the sorting of different kinds on the dashboard data.

## Endpoint Description
` PORT = current port on which app is running (3002 if run locally)`

## Dashboard
```
 http://localhost:PORT/dashboard
```

### API for paginated list of videos
```cuRL
 http://localhost:PORT/api/videos?skip=&limit=
```
This api lists all the videos present in DB, in a paginated formated, having skip and limit as query params.

### API for searching video based on title or description text 
```
 http://localhost:PORT/api/search/video?searchQuery=
```
This api lists all the videos present in DB, in which the title or the description has some text matching with searchQuery.

---
# IMPORTANT!
```
- The app might give error and not run, while running using Docker. It is because the api key collection in DB is re-created everytime. To tackle that issue, I've created an endpoint which populates some new keys into it. 
- Hit this url once, and then the app should work fine.
```
 https://setkeystodb.herokuapp.com/
