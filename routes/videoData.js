// This file contains the video data related endpoint routes.

const router = require('express').Router();
const {getVideoDetails , searchVideos}  =require('../core/videoData');

// api to get the list of all videos from DB in paginated format.
router.get('/videos', getVideoDetails);

// to search for a videos based on title or description text match.
router.get('/search/video', searchVideos);

module.exports = router;