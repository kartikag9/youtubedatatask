/*

This is the major core of the application, it basically runs after every 10 seconds and look for latest videos from youtube based on the query *Cricket* 
and exports the data into the database.

We keep a api key in our cache storage and a couple of keys which are new and unused into the database. Whenever the quota is exhausted or the api gives a bad request(in case the api key is wrong)
then it will automatically fetch a new API key from the DB and replace the one in the local storage with the new one. This way we do not need to worry about the api key.
 
*/

let storage = require('node-persist');
let myStorage = storage.create({dir: 'myDir', ttl: 300000});

let videoDataSchema = require('../models/videoData')
let apiKeySchema = require("../models/apiKey");
const request = require('request');

var query = "cricket"
var callAfter = 10000 
var maxResults = 25
var publishedAfter = "2021-01-01T00:00:00Z"

setInterval(async()=>{
        await myStorage.init();
        var key = await myStorage.getItem('key')
        console.log("current key: " + key)
    request({
        url:   `https://youtube.googleapis.com/youtube/v3/search?order=date&q=${query}&type=video&key=${key}&maxResults=${maxResults}&publishedAfter=${publishedAfter}`,
        method: "GET",
        timeout: 10000,
        followRedirect: true,
        maxRedirects: 10
    },function(error, response, body){
        if(!error && response.statusCode == 200){
            console.log('sucess!');
            var parsedBody = JSON.parse(body)
            var videoIds = [];
            parsedBody.items.forEach(element => {
                videoIds.push(element.id.videoId)
            });
            request({
                url: `https://www.googleapis.com/youtube/v3/videos?id=${videoIds}&key=${key}&part=snippet,contentDetails,statistics,status`,
                method: "GET",
                timeout: 10000,
                followRedirect: true,
                maxRedirects: 10
            }, (err, resp, data)=> {
                if(!err && resp.statusCode == 200) {
                    var parsedData = JSON.parse(data); 
                    parsedData.items.forEach(ele => {
                        doc = {"title": ele.snippet.title, "description": ele.snippet.description, "publishedAt": Date.parse(ele.snippet.publishedAt), "thumbnails": ele.snippet.thumbnails};
                        videoDataSchema.create(doc, (errr,docs)=> {
                            if(errr && errr.code != 11000) {
                                console.log("error in saving video data to DB: " + errr)
                            } else {
                                console.log("data exported to db");
                            }
                        });
                    });
                } else {
                    console.log('error: ' + resp.statusCode);
                }
            })
        }else{
            if(response.statusCode == 403 || response.statusMessage == "Forbidden" || response.statusMessage == "Bad Request") {
                // quota exhausted or Bad request, api key changing
                    apiKeySchema.find({}).sort('SNO').exec((err,docs) => {
                        if(!err) {
                            console.log("newKey: " + docs[0].key);
                            myStorage.setItem('key',docs[0].key);
                            deleteExpiredKeyFromDB(docs[0].key);
                        } else {
                            console.log(err)
                        }
                    });
            }
            console.log('error: ' + response.statusMessage);
        }
    });
}, callAfter);


//  all keys in DB are new, so once we pick a key from DB and put it into our myStorage cache, then, removed it from DB.
function deleteExpiredKeyFromDB(usedKey) {
    apiKeySchema.findOneAndDelete({"key": usedKey}, (err,docs)=> {
        if(!err) {
            console.log("Deleted used Key");
        } else {
            console.log("Error in Deleting used Key");
        }
    })
};