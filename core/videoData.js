// This contains the controllers to which the endpoint routes are connected to.

const express = require('express');
const videoDataSchema = require('../models/videoData');

// to fetch all the videos present in the DB, in paginated format
exports.getVideoDetails = (req,res) => {
    let skip = parseInt(req.query.skip);
    let limit = parseInt(req.query.limit);
    videoDataSchema.find({}).skip(skip).limit(limit).sort(-"publisedAt").exec((err,docs)=>{
        if(err) {
            return res.json({"error": err})
        }
        return res.json(docs);
    });
};

// search videos based on searchQuery, matches with both title and description
exports.searchVideos = (req,res) => {
    const regex = new RegExp(escapeRegex(req.query.searchQuery), 'gi');
    videoDataSchema.find({$or:[{"title": regex},{"description": regex}]}).exec((err,docs)=>{
        if(err) {
            return res.json({"error": err})
        }
        return res.json(docs);
    });
};

var escapeRegex = (text) => {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
};